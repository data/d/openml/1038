{
  "data_set_description": {
    "creator": "Isabelle Guyon",
    "default_target_attribute": "label",
    "description": "**Author**: [Isabelle Guyon](isabelle@clopinet.com)  \n**Source**: [Agnostic Learning vs. Prior Knowledge Challenge](http://www.agnostic.inf.ethz.ch)  \n**Please cite**: None\n\n\nDataset from the Agnostic Learning vs. Prior Knowledge Challenge (http://www.agnostic.inf.ethz.ch), which consisted of 5 different datasets (SYLVA, GINA, NOVA, HIVA, ADA). The purpose of the challenge was to check if the performance of domain-specific feature engineering (prior knowledge) can be met by algorithms that were trained on data without any domain-specific knowledge (agnostic). For the latter, the data was anonymised and preprocessed in a way that makes them uninterpretable. \n\nModified by TunedIT (converted to ARFF format)\n\n\n\n### Topic\n\nThe task of GINA is handwritten digit recognition. This is the agnostic version of a subset of the MNIST data set. We chose the problem of separating the odd numbers from even numbers. We use 2-digit numbers. Only the unit digit is informative for that task, therefore at least ½ of the features are distracters. This is a twoclass classification problem with sparse continuous input variables, in which each class is\ncomposed of several clusters. It is a problems with heterogeneous classes.\n\n\n### Source\n\nThe data set was constructed from the MNIST data that is made available by Yann\nLeCun of the NEC Research Institute at [http://yann.lecun.com/exdb/mnist/](http://yann.lecun.com/exdb/mnist/). The digits have been size-normalized and centered in a fixed-size image of dimension 28x28. Examples are shown in the [documentation in chapter 3](http://clopinet.com/isabelle/Projects/agnostic/Dataset.pdf).\n\n\n### Description \n\nTo construct the “agnostic” dataset, we performed the following steps:\n- We removed the pixels that were 99% of the time white. This reduced the original\nfeature set of 784 pixels to 485.\n- The original resolution (256 gray levels) was kept.\n- In spite of the fact that the data are rather sparse (about 30% of the values are\nnon-zero), we saved the data as a dense matrix because we found that it can be\ncompressed better in this way (to 19 MB.)\n- The feature names are the (i,j) matrix coordinates of the pixels (in a 28x28\nmatrix.)\n- We created 2 digit numbers by dividing the datasets into to parts and pairing the\ndigits at random.\n- The task is to separate odd from even numbers. The digit of the tens being not\ninformative, the features of that digit act as distracters.\nTo construct the “prior” dataset, we went back to the original data and fetched the\n“informative” digit in its original representation. Therefore, this data representation\nconsists in a vector of concatenating the lines of a 28x28 pixel map.\n\nData type: non-sparse  \nNumber of features: 970  \nNumber of examples and check-sums:  \nPos_ex Neg_ex Tot_ex Check_sum  \nTrain  1550  1603  3153 164947945.00  \nValid   155   160   315 16688946.00  \n\n\nThis dataset contains samples from both training and validation datasets.",
    "description_version": "1",
    "file_id": "53921",
    "format": "ARFF",
    "id": "1038",
    "language": "English",
    "licence": "Public",
    "md5_checksum": "e8b42f51e0a453e4b77c24f8e2569948",
    "minio_url": "https://data.openml.org/datasets/0000/1038/dataset_1038.pq",
    "name": "gina_agnostic",
    "original_data_url": "http://www.agnostic.inf.ethz.ch",
    "parquet_url": "https://data.openml.org/datasets/0000/1038/dataset_1038.pq",
    "processing_date": "2020-11-20 20:37:19",
    "status": "active",
    "tag": [
      "derived",
      "Education",
      "mythbusting_1",
      "OpenML100",
      "study_1",
      "study_123",
      "study_135",
      "study_14",
      "study_15",
      "study_20",
      "study_34",
      "study_41"
    ],
    "upload_date": "2014-10-06T23:56:01",
    "url": "https://api.openml.org/data/v1/download/53921/gina_agnostic.arff",
    "version": "1",
    "visibility": "public"
  }
}